<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>
<body>

<?php

// Ecrire le code permettant d'afficher un message lorsque : 
// L'age de l'utilisateur est compris entre 0 et 17 ans : Trop jeune bonhomme
// L'age de l'utilisateur est compris entre 18 et 21 ans : Majeur mais pas adulte
// L'age de l'utilisateur est compris entre 22 et 55 ans : Adulte et apte au travail
// L'utilisateur à plus de 55 ans : Courage mon cher, la retraite arrive.
// Si l'age est supérieur à 62 ans : Bonne retraite
    
// Dans la même condition, suivant si c'est un homme ou une femme, le texte changera : 
// 0 - 18 ans : Trop jeune, jeune fille
// L'utilisatrice à plus de 55 ans : Courage madame, la retraite arrive.
    
// Si c'est une femme, que son age est compris entre 18 et 40 ans, elle peux avoir un congé maternité

$genre = ["Homme", "Femme"];
$genre2 = rand(0, 1);
    
$age = rand(0, 70);
       
?>
<!-- écrire le code après ce commentaire -->

<h1><?php echo $genre[$genre2]."<br>";?></h1>
<h2><?php echo $age."<br>";?></h2>

<h3><?php
if($age <=17){
    if($genre2 == 1){
        echo "Trop jeune, jeune fille";
    } else {
        echo "Trop jeune, bonhomme";
    }
} elseif($age >=18 AND $age <=21) {
    if($genre2 == 1){
        echo "Majeure mais pas adulte. Peut avoit un congé maternité.";
    } else {
        echo "Majeur mais pas adulte.";
    }
} elseif($age >=22 AND $age <=40) {
    if($genre2 == 1){
        echo "Adulte et apte au travail.Peut avoit un congé maternité.";
    } else {
        echo "Adulte et apte au travail.";
    }
} elseif($age >=41 AND $age <=55) {
    echo "Adulte et apte au travail.";
} elseif($age >=56 AND $age <=62) {
    if($genre2 == 1){
        echo "Courage madame, la retraite arrive.";
    } else {
        echo "Courage mon cher, la retraite arrive.";
    }
} elseif($age >=63) {
    echo "Bonne retraite";
}

else {
    echo "Hors norme";
}

?></h3>


<!-- écrire le code avant ce commentaire -->

</body>
</html>
