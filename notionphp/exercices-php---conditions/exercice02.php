<!DOCTYPE html>
<!-- Exercice PHP - CodeColliders 2020 - https://codecolliders.com -->
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>
<body>
<ul>
    <li><a href="?mot=muffin">muffin</a></li>
    <li><a href="?mot=jelly-beans">jelly beans</a></li>
    <li><a href="?mot=chocolate cake">chocolate cake</a></li>
    <li><a href="?mot=donut">donut</a></li>
    <li><a href="?mot=ice-cream">ice cream</a></li>
</ul>
    <?php

    $unNombreAleatoire = mt_rand(0, 50)
    // en utilisant la variable : $_GET['mot'] qui permet de récupérer la variable d'url "mot",
    // afficher "mot court" si le mot fait moins de 6 caracteres et "mot long"  dans le cas contraire
    ?>
    <!-- écrire le code après ce commentaire -->
    <?php
    echo $_GET["mot"]."<br>";
     if ( strlen ($_GET['mot'])<=6) {
         echo "mot court";
     }
     else{
         echo "mot long";
     } 
    ?>  
    <!-- écrire le code avant ce commentaire -->

</body>
</html>
